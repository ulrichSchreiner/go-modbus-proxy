# Modbus proxy

This is a quick hack for my solaredge inverter. It has a modbus interface but only
supports one single client connection at a time.

To support more than one client i wrote this proxy; it dispatches multiple clients to one
single backend connection.

Use at your own risk!

## Usage

~~~
registry.gitlab.com/ulrichschreiner/go-modbus-proxy -listen 0.0.0.0:1502 -target 192.168.0.8:502 -timeout 5s -clienttimeout 30s
~~~

Opens a local listener on port 1502 and forwards the requests to the target
`192.168.0.8:502`. The `timeout` is used for the target connection, so your modbus service
should respond within this timeouts after q request is sent to the service.

The `clienttimeout` is used for client connections.