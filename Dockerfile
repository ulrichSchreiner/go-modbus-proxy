FROM registry.gitlab.com/ulrichschreiner/go-web-dev as builder

ADD . /work
WORKDIR /work
RUN make build

FROM registry.gitlab.com/ulrichschreiner/base/ubuntu:22.04
COPY --from=builder /work/modbus-proxy /modbus-proxy
ENTRYPOINT ["/modbus-proxy"]