package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

type mbap struct {
	TransactionID uint16
	ProtocolID    uint16
	Length        uint16
	UnitID        byte
}

type mbdata struct {
	header mbap
	data   []byte
	err    error
}

type mbflow struct {
	rq  mbdata
	rsp chan mbdata
}

func newFlow(h mbap, data []byte) *mbflow {
	return &mbflow{
		rq: mbdata{
			header: h,
			data:   data,
		},
		rsp: make(chan mbdata),
	}
}

func main() {
	listen := flag.String("listen", "127.0.0.1:1502", "the listen address of the proxy")
	target := flag.String("target", "127.0.1.1:502", "the target address of the modbus service")
	timeout := flag.Duration("timeout", 3*time.Second, "timeout for target server")
	clienttimeout := flag.Duration("clienttimeout", 30*time.Second, "timeout for client connection")

	flag.Parse()

	mp := newProxy(*listen, *target, *timeout, *clienttimeout)
	err := mp.start()
	if err != nil {
		log.Fatal(err)
	}
}

type modbusProxy struct {
	listen        string
	target        string
	timeout       time.Duration
	clienttimeout time.Duration
}

func newProxy(listen, ta string, timeout, cltimeout time.Duration) *modbusProxy {
	return &modbusProxy{listen: listen, target: ta, timeout: timeout, clienttimeout: cltimeout}
}

func (mp *modbusProxy) start() error {
	flow, err := mp.startTargetService()
	if err != nil {
		log.Fatal(err)
	}
	la, err := net.Listen("tcp", mp.listen)
	if err != nil {
		log.Fatal(err)
	}
	// close listener
	defer la.Close()
	for {
		conn, err := la.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go mp.handleIncomingRequest(flow, conn)
	}
}

func (mp *modbusProxy) startTargetService() (chan<- mbflow, error) {
	flow := make(chan mbflow)

	var srv net.Conn
	defer func() {
		if srv != nil {
			srv.Close()
		}
	}()

	reset := func() {
		if srv != nil {
			srv.Close()
		}
		srv = nil
		s, err := net.Dial("tcp", mp.target)
		if err != nil {
			log.Printf("cannot connect to backend: %s: %v", mp.target, err)
			return
		}
		log.Printf("connected to backend: %s", mp.target)
		srv = s
	}
	reset()
	if srv == nil {
		return nil, fmt.Errorf("cannot connect to backend")
	}

	go func() {
		for f := range flow {
			if srv == nil {
				f.rsp <- mbdata{err: fmt.Errorf("no backend connection")}
				reset()
				continue
			}
			_ = srv.SetDeadline(time.Now().Add(mp.timeout))
			log.Printf("new target request: %+v (%+v)", f.rq.header, f.rq.data)
			err := binary.Write(srv, binary.BigEndian, f.rq.header)
			if err != nil {
				f.rsp <- mbdata{err: fmt.Errorf("cannot write header to target: %w", err)}
				reset()
				continue
			}
			_ = srv.SetDeadline(time.Now().Add(mp.timeout))
			_, err = srv.Write(f.rq.data)
			if err != nil {
				f.rsp <- mbdata{err: fmt.Errorf("cannot write data to target: %w", err)}
				reset()
				continue
			}

			_ = srv.SetDeadline(time.Now().Add(mp.timeout))
			var srvheader mbap
			err = binary.Read(srv, binary.BigEndian, &srvheader)
			if err != nil {
				f.rsp <- mbdata{err: fmt.Errorf("cannot read header from target: %w", err)}
				reset()
				continue
			}
			_ = srv.SetDeadline(time.Now().Add(mp.timeout))
			buffer := make([]byte, srvheader.Length-1)
			_, err = srv.Read(buffer)
			if err != nil {
				f.rsp <- mbdata{err: fmt.Errorf("cannot read data from target: %w", err)}
				reset()
				continue
			}
			log.Printf("new target response: %+v (%+v)", srvheader, buffer)
			f.rsp <- mbdata{header: srvheader, data: buffer}
			_ = srv.SetDeadline(time.Time{}) // disable timeout
		}
	}()

	return flow, nil
}

func (mp *modbusProxy) handleIncomingRequest(flow chan<- mbflow, conn net.Conn) {
	log.Printf("new client: %s", conn.RemoteAddr().String())
	defer func() {
		log.Printf("close connection: %s", conn.RemoteAddr().String())
		conn.Close()
	}()

	for {
		var header mbap
		_ = conn.SetDeadline(time.Now().Add(mp.clienttimeout))
		err := binary.Read(conn, binary.BigEndian, &header)
		if err != nil {
			if err != io.EOF {
				log.Printf("cannot read header from client: %v", err)
			}
			return
		}
		buffer := make([]byte, header.Length-1) // the length also contains the UnitID

		_, err = conn.Read(buffer)
		if err != nil {
			log.Printf("cannot read data buffer: %v", err)
			return
		}

		f := newFlow(header, buffer)
		flow <- *f
		rsp := <-f.rsp
		log.Printf("received %+v", rsp)
		if rsp.err != nil {
			log.Printf("target error: %v", rsp.err)
			return
		}

		err = binary.Write(conn, binary.BigEndian, rsp.header)
		if err != nil {
			log.Printf("cannot write header to client: %v", err)
			return
		}
		_, err = conn.Write(rsp.data)
		if err != nil {
			log.Printf("cannot write data to client: %v", err)
			return
		}

		_ = conn.SetDeadline(time.Time{})
		log.Printf("header: %#v, data: %+v", rsp.header, rsp.data)
	}
}
